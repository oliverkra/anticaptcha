package anticaptcha

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

var (
	baseURL = &url.URL{Host: "api.anti-captcha.com", Scheme: "https", Path: "/"}

	Debug = false
)

type Client struct {
	APIKey string
}

type createTaskRecaptchaResponse struct {
	TaskID float64 `json:"taskId"`
}

// Method to create the task to process the recaptcha, returns the task_id
func (c *Client) createTaskRecaptcha(websiteURL string, recaptchaKey string) (float64, error) {
	if Debug {
		log.Printf("ANTI-CAPTCHA REQUEST: %s > %s", websiteURL, recaptchaKey)
	}

	// Mount the data to be sent
	body := map[string]interface{}{
		"clientKey": c.APIKey,
		"task": map[string]interface{}{
			"type":       "NoCaptchaTaskProxyless",
			"websiteURL": websiteURL,
			"websiteKey": recaptchaKey,
		},
	}

	b, err := json.Marshal(body)
	if err != nil {
		return 0, err
	}

	// Make the request
	u := baseURL.ResolveReference(&url.URL{Path: "/createTask"})
	resp, err := http.Post(u.String(), "application/json", bytes.NewBuffer(b))
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	if Debug {
		log.Printf("ANTI-CAPTCHA RESPONSE: [%d] %s", resp.StatusCode, string(content))
	}

	// Decode response
	responseBody := createTaskRecaptchaResponse{}
	if err := json.Unmarshal(content, &responseBody); err != nil {
		return 0, err
	}

	// TODO treat api errors and handle them properly
	return responseBody.TaskID, nil
}

type getTaskResultResponse struct {
	Status   string `json:"status"`
	Solution struct {
		GRecaptchaResponse string `json:"gRecaptchaResponse"`
		Text               string `json:"text"`
	} `json:"solution"`

	ErrorID          int64  `json:"errorId"`
	ErrorDescription string `json:"errorDescription"`
}

// Method to check the result of a given task, returns the json returned from the api
func (c *Client) getTaskResult(taskID float64) (*getTaskResultResponse, error) {
	// Mount the data to be sent
	body := map[string]interface{}{
		"clientKey": c.APIKey,
		"taskId":    taskID,
	}
	b, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	// Make the request
	u := baseURL.ResolveReference(&url.URL{Path: "/getTaskResult"})
	resp, err := http.Post(u.String(), "application/json", bytes.NewBuffer(b))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if Debug {
		log.Printf("ANTI-CAPTCHA REQUEST: [%d] %s", resp.StatusCode, string(content))
	}

	// Decode response
	responseBody := &getTaskResultResponse{}
	if err := json.Unmarshal(content, &responseBody); err != nil {
		return nil, err
	}
	if responseBody.ErrorID > 0 {
		return nil, errors.New(responseBody.ErrorDescription)
	}
	return responseBody, nil
}

// SendRecaptcha Method to encapsulate the processing of the recaptcha
// Given a url and a key, it sends to the api and waits until
// the processing is complete to return the evaluated key
func (c *Client) SendRecaptchaWithTimeAndRetries(websiteURL string, recaptchaKey string, retries int, sleep time.Duration) (string, error) {
	// Create the task on anti-captcha api and get the task_id
	taskID, err := c.createTaskRecaptcha(websiteURL, recaptchaKey)
	if err != nil {
		return "", err
	}

	time.Sleep(sleep)
	for i := 1; i <= retries; i++ {
		response, err := c.getTaskResult(taskID)
		if err != nil {
			return "", err
		}

		if response.Status == "processing" {
			log.Println("Result is not ready, waiting a few seconds to check again...")
			time.Sleep(sleep)
			continue
		}

		return response.Solution.GRecaptchaResponse, nil
	}
	return "", errors.New("TIMEOUT")
}

// SendRecaptcha Method to encapsulate the processing of the recaptcha
// Given a url and a key, it sends to the api and waits until
// the processing is complete to return the evaluated key
func (c *Client) SendRecaptcha(websiteURL string, recaptchaKey string) (string, error) {
	return c.SendRecaptchaWithTimeAndRetries(websiteURL, recaptchaKey, 5, time.Second*5)
}
